import React, { Component } from 'react';
import { BrowserRouter as Router,Routes, Route, Link } from 'react-router-dom';
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Login from './components/Login'
import Registration from './components/Registration';
import './App.css';

class App extends Component {
render() {
	return (
	<Router>
		<div className="App">
			
		<Routes>
				<Route exact path='/' element={< Home />}></Route>
				<Route exact path='/login' element={< Login />}></Route>
				<Route exact path='/register' element={< Registration />}></Route>
		</Routes>
		</div>
	</Router>
);
}
}

export default App;
