import React from "react";
import { Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import "./Registration.css";
import "bootstrap/dist/css/bootstrap.min.css";
import DropdownMultiselect from "react-multiselect-dropdown-bootstrap";
import Select from 'react-select';
class Registration extends React.Component {
  constructor() {
    super();
    this.state = {
      input: {},
      errors: {},
      selected:null,
      selectedCourse:null,
      selectedGender:null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    let input = this.state.input;
    input[event.target.name] = event.target.value;
    
    this.setState({
      input,
    });
  }
  handleoption = selected =>
  {
    this.setState({selected});
    console.log(`Option selected:`, selected);
  };

  handleCourse = selectedCourse =>
  {
    this.setState({selectedCourse});
    console.log(`Option selected:`, selectedCourse);
  };

  handleGender = selectedGender =>
  {
    this.setState({selectedGender});
    console.log(`Option selected:`, selectedGender.value);
  };

  handleSubmit(event) {
    event.preventDefault();

    if (this.validate()) {
      console.log(this.state);

      let input = {};
      input["username"] = "";
      input["password"] = "";
      input["phonenumber"] = "";
      input["middlename"] = "";
      input["lastname"] = "";
      input["dob"] = "";
      input["email"] = "";
      input["about"] = "";
      input["gender"] = "";
      input["skills"] = "";
      input["course"] = "";
      this.setState({ input: input });

      alert("Registration Successful");
    }
  }

  validate() {
    let input = this.state.input;
    let selected = this.state.selected;
    let selectedCourse = this.state.selectedCourse;
    let selectedGender = this.state.selectedGender;
    let errors = {};
    let isValid = true;

    if (!input["username"]) {
      isValid = false;
      errors["username"] = "Please enter your Username.";
    }
   

    if (!input["phonenumber"]) {
      isValid = false;
      errors["phonenumber"] = "Please enter your Phonenumber.";
    }

    if (input["phonenumber"].length > 10) {
      isValid = false;
      errors["phonenumber"] = "Please enter valid phonenumber.";
    }

    if (!input["lastname"]) {
      isValid = false;
      errors["lastname"] = "Please enter your lastname.";
    }

    if (!input["email"]) {
      isValid = false;
      errors["email"] = "Please enter your email.";
    }

    if (!input["dob"]) {
      isValid = false;
      errors["dob"] = "Please enter your date of birth.";
    }

    if (!input["about"]) {
      isValid = false;
      errors["about"] = "Please enter about yourself.";
    }
    if(!selected)
    {
      isValid = false;
      errors["skills"] = "Please select your skills.";
    }
    if(!selectedCourse)
    {
      isValid = false;
      errors["course"] = "Please select your Course.";
    }

    this.setState({
      errors: errors,
    });

    return isValid;
  }
  
  Countries = [
    { label: "Java", value: 1 },
    { label: "Python", value: 2 },
    { label: "Reactjs", value: 3 },
    { label: "RESTAPI", value: 4 },
    { label: "AWS", value: 5 },
    { label: "Microservices", value: 6 },
    { label: "Database", value: 7 }
  ];

  Courses = [
    { label: "BE Computer Science", value: "CSE" },
    { label: "BE Mechanical", value: "MECH" },
    { label: "BE Electrical", value: "ECE" },
    { label: "BE Information Technology", value: "IT" },
    { label: "BE Civil", value: "CIVIL" }
  ];

  render() {
    const { selected,selectedCourse,selectedGender } = this.state;

    return (

      <div className="App d-flex flex-column align-items-center">
        <h1>Registration Page</h1>
        <Form onSubmit={this.handleSubmit}>
          <div class="form-group required">
            <Form.Group className="mb-3" controlId="formBasicFirstName">
              <Form.Label className="control-label">First Name</Form.Label>
              <Form.Control
                type="text"
                name="username"
                value={this.state.input.username}
                onChange={this.handleChange}
                placeholder="Enter First name"
              />
              <div className="text-danger">{this.state.errors.username}</div>
            </Form.Group>
          </div>

          <Form.Group className="mb-3" controlId="formBasicMiddleName">
            <Form.Label>Middle Name</Form.Label>
            <Form.Control
              type="text"
              value={this.state.input.middlename}
              onChange={this.handleChange}
              placeholder="Enter Middle name"
            />
          </Form.Group>

          <div class="form-group required">
            <Form.Group className="mb-3" controlId="formBasicLastName">
              <Form.Label className="control-label">Last Name</Form.Label>
              <Form.Control
                type="text"
                name="lastname"
                value={this.state.input.lastname}
                onChange={this.handleChange}
                placeholder="Enter Last name"
              />
              <div className="text-danger">{this.state.errors.lastname}</div>
            </Form.Group>
          </div>

          <div class="form-group required">
            <Form.Group className="mb-5" controlId="formGender">
              <Form.Label className="control-label">Gender</Form.Label>
              {["Male", "Female"].map((values) => (
                <Form.Check
                  label={values}
                  name="gender"
                  type="radio"
                  id={`inline-radio-1`}
                  required
                />
              ))}
            </Form.Group>
          </div>

          <div class="form-group required">
            <Form.Group className="mb-5" controlId="formGender">
              <Form.Label className="control-label"> Please Gender</Form.Label>
              {["Male", "Female"].map((values) => (
                <Form.Check
                  label={values}
                  name="gender"
                  type="radio"
                  value={selectedGender}
                  onChange={this.handleGender}
                />
              ))}
            </Form.Group>
          </div>

          <div class="form-group required">
            <Form.Group className="mb-3" controlId="formDateOfBiirth">
              <Form.Label className="control-label">Date of Birth</Form.Label>
              <Form.Control
                type="date"
                name="dob"
                placeholder="Date of Birth"
                value={this.state.input.dob}
                onChange={this.handleChange}
              ></Form.Control>
              <div className="text-danger">{this.state.errors.dob}</div>
            </Form.Group>
          </div>

          <div class="form-group required">
            <Form.Group className="mb-3" controlId="formBasicLastName">
              <Form.Label className="control-label">Email</Form.Label>
              <Form.Control
                type="email"
                name="email"
                value={this.state.input.email}
                onChange={this.handleChange}
                placeholder="Enter Email"
              />
              <div className="text-danger">{this.state.errors.email}</div>
            </Form.Group>
          </div>

          <div class="form-group required">
            <Form.Group className="mb-3" controlId="formBasicLastName">
              <Form.Label className="control-label">Phone</Form.Label>
              <Form.Control
                type="number"
                name="phonenumber"
                value={this.state.input.phonenumber}
                onChange={this.handleChange}
                placeholder="Enter Phonenumber"
              />
              <div className="text-danger">{this.state.errors.phonenumber}</div>
            </Form.Group>
          </div>

          {/*<div class="form-group required">
            <Form.Label className="control-label">
              Select your Department
            </Form.Label>
            <br />
            <select required>
              <option value="">Please Select your Department</option>
              <option value="CSE">BE Computer Science</option>
              <option value="MECH">BE Mechanical</option>
              <option value="ECE">BE Electrical</option>
              <option value="IT">BE Information Technology</option>
            </select>
          </div>*/}

          <div class="form-group required">
            <Form.Group className="mb-3" controlId="skills">
              <br />
              <Form.Label className="control-label">
                Select your Courses
              </Form.Label>
          <Select options={this.Courses} 
          value={selectedCourse}
          onChange={this.handleCourse}
            />
          <div className="text-danger">{this.state.errors.course}</div>
          </Form.Group>
              </div>

          <div class="form-group required">
            <Form.Group className="mb-3" controlId="skills">
              <br />
              <Form.Label className="control-label">
                Select your skills
              </Form.Label>
          <Select options={this.Countries} 
          isMulti={true}
          value={selected}
          onChange={this.handleoption}
            />
          <div className="text-danger">{this.state.errors.skills}</div>
          </Form.Group>
              </div>

          {/*<div class="form-group required">
            <Form.Group className="mb-3" controlId="skills">
              <br />
              <Form.Label className="control-label">
                Select your skills
              </Form.Label>
              <DropdownMultiselect
                options={[
                  "Java",
                  "Python",
                  "Reactjs",
                  "RESTAPI",
                  "Microservices",
                  "AWS",
                ]}
                name="skills"
                value={this.state.input.skills}
                onChange={this.handleChange}/>
                
            </Form.Group>
              </div>*/}

          <div class="form-group required">
            <Form.Group className="mb-3" controlId="Textarea">
              <Form.Label className="control-label">
                Tell me about Yourself
              </Form.Label>
              <Form.Control
                as="textarea"
                name="about"
                rows={3}
                placeholder="I am Senthil Working as a Java Developer in TCS for past 3 years"
                value={this.state.input.about}
                onChange={this.handleChange}
              />
              <div className="text-danger">{this.state.errors.about}</div>
            </Form.Group>
          </div>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}
export default Registration;
