import React from "react";
import { Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form'
import 'bootstrap/dist/css/bootstrap.min.css';
class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      input: {},
      errors: {}
    };
     
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
     
  handleChange(event) {
    let input = this.state.input;
    input[event.target.name] = event.target.value;
  
    this.setState({
      input
    });
  }
     
  handleSubmit(event) {
    event.preventDefault();
  
    if(this.validate()){
        console.log(this.state);
  
        let input = {};
        input["username"] = "";
        input["password"] = "";
        this.setState({input:input});
  
        alert('Login Successful');
        window.location.href = "http://localhost:3000/register";
        //location.replace('/register');
    }
  }
  
  validate(){
      let input = this.state.input;
      let errors = {};
      let isValid = true;
   
      if (!input["username"]) {
        isValid = false;
        errors["username"] = "Please enter your username.";
      }
  
      if (typeof input["username"] !== "undefined") {
        const re = /^\S*$/;
        if(input["username"].length < 6 || !re.test(input["username"])){
            isValid = false;
            errors["username"] = "Please enter valid username.";
        }
      }
  
     
      if (!input["password"]) {
        isValid = false;
        errors["password"] = "Please enter your password.";
      }
  
      if (typeof input["password"] !== "undefined") {
        const passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
        if(!passw.test(input["password"])){
            isValid = false;
            errors["password"] = "Please Type the correct Password Format.";
        }
      }
      
  
      this.setState({
        errors: errors
      });
  
      return isValid;
  }
     
  render() {
    return (
      <div className='App d-flex flex-column align-items-center'>
        <h1>Login Page</h1>

        <form onSubmit={this.handleSubmit}>
  
          <div class="form-group">
            <label for="username">Username:</label>
            <input 
              type="text" 
              name="username" 
              value={this.state.input.username}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter username" 
              id="username"
               />
  <div className="text-danger">{this.state.errors.username}</div>
          </div>
          <br/>
  
          
  
          <div class="form-group">
            <label for="password">Password:</label>
            <input 
              type="password" 
              name="password" 
              value={this.state.input.password}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter password" 
              id="password" />
              <Form.Text className="text-muted">
    Password must be 6-20 characters long and contain one uppercase and one lowercase character
    </Form.Text>
  
              <div className="text-danger">{this.state.errors.password}</div>
          </div>
  
          
             <br/>
          <input type="submit" value="Submit" class="btn btn-primary" />
        </form>

      </div>
    );
  }

  
}
export default Login