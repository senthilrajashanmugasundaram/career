import React from 'react';
import { BrowserRouter as Router,Routes, Route, Link } from 'react-router-dom';

class Home extends React.Component
{
    render() {
        return (
            <div>
                <h1>Welcome to the world of Job portal!</h1>
                <h2>Click Login to join our Portal</h2>
                <Link to="/login" className="btn btn-primary">login</Link>
                </div>
        );	 
}
}

export default Home;

